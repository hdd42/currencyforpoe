Orb_of_Scouring       : 10/17    -  10/1      Profit: 48,824
Apprentice_Sextant    : 7/5      -  2/1       Profit: 3,600
Regal_Orb             : 1/1      -  12/10     Profit: 2,400
Divine_Orb            : 2/13     -  8/1       Profit: 1,846
Orb_of_Regret         : 30/19 +  -  13/18 +   Profit: 1,825
Vaal_Orb              : 29/10    -  5/11      Profit: 1,591
Orb_of_Transmutation  : 59/1     -  1/25      Profit: 1,360
Exalted_Orb           : 1/29     -  30/1      Profit: 1,034
Blessed_Orb           : 4/1      -  1/2       Profit: 1,000
Orb_of_Chance         : 59/8 +   -  5/31 +    Profit: 0,948
Orb_of_Fusing         : 33/10    -  10/31     Profit: 0,645
Orb_of_Alteration     : 17/1     -  10/160    Profit: 0,625
Cartographer's_Chisel : 4/1      -  3/10 +    Profit: 0,600
Gemcutter's_Prism     : 14/10    -  1/1       Profit: 0,400
Orb_of_Alchemy        : 11/2     -  6/31      Profit: 0,387
Perandus_Coin         : 200/1    -  1/150     Profit: 0,333
Chromatic_Orb         : 113/10   -  10/110    Profit: 0,273
Jeweller's_Orb        : 128/10   -  5/61 +    Profit: 0,246
Silver_Coin           : 169/10 + -  1/14      Profit: 0,207
