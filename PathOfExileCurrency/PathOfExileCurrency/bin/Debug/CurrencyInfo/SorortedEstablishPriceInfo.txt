Orb_of_Scouring       : 9/16     -  33/4      Profit: 120,141
Apprentice_Sextant    : 11/8     -  19/10     Profit: 30,638
Orb_of_Transmutation  : 589/10   -  10/251    Profit: 13,466
Blessed_Orb           : 39/10    -  10/21     Profit: 8,571
Gemcutter's_Prism     : 11/8     -  20/21     Profit: 6,190
Perandus_Coin         : 1999/10  -  10/1501   Profit: 3,318
Vaal_Orb              : 23/8     -  9/20      Profit: 2,644
Silver_Coin           : 169/10 + -  10/141    Profit: 1,986
Orb_of_Regret         : 30/19 +  -  13/18 +   Profit: 1,825
Orb_of_Chance         : 59/8 +   -  5/31 +    Profit: 0,948
Regal_Orb             : 19/20    -  7/6       Profit: 0,758
Cartographer's_Chisel : 39/10    -  3/10 +    Profit: 0,510
Orb_of_Alteration     : 169/10   -  10/161    Profit: 0,497
Orb_of_Fusing         : 13/4     -  8/25      Profit: 0,320
Jeweller's_Orb        : 115/9    -  5/61 +    Profit: 0,237
Orb_of_Alchemy        : 49/9     -  5/26      Profit: 0,235
Chromatic_Orb         : 45/4     -  10/111    Profit: 0,135
