﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngleSharp.Css.Values;

namespace PathOfExileCurrency
{
    class Currency
    {
        public double PurchasePrice;
        public double SellingPrice;
        public double SizeOfStock;
        public string Name;
        public Boolean IsGoodTrader;
        public Currency(double purchasePrice, double sellingPrice, double sizeOfStock,string name)
        {
            PurchasePrice = purchasePrice;
            SellingPrice = sellingPrice;
            SizeOfStock = sizeOfStock;
            Name = name;
            IsGoodTrader = false;
        }
        public Currency(double purchasePrice, double sellingPrice, double sizeOfStock, string name, Boolean isGoodTrader)
        {
            PurchasePrice = purchasePrice;
            SellingPrice = sellingPrice;
            SizeOfStock = sizeOfStock;
            Name = name;
            IsGoodTrader = isGoodTrader;
        }

        public Currency()
        {
            PurchasePrice = 1;
            SellingPrice = 1;
            SizeOfStock = 1;
            Name = "Error";
        }

        public Boolean CheckStock()
        {
            return SizeOfStock > 5;
        }

        public double GetPrice()
        {
            return PurchasePrice / SellingPrice;
        }
        public string GetInfo()
        {
            return "PurPrice: " + PurchasePrice + " : "
                    + "SelPrice: " + SellingPrice
                    + " Stock: " + SizeOfStock;
        }
        public string GetShortInfo()
        {
            string info = PurchasePrice + "/"
                   + SellingPrice;
            return GetTable(info, 8);
        }
        public string GetReverseShortInfo()
        {
            string info = SellingPrice + "/"
                   + PurchasePrice;
            return GetTable(info,9);
        }

        private string GetTable(string Info,int n)
        {
            string info = Info;
            if (IsGoodTrader == true) info += " +";
            if (info.Length < n)
            {
                while (info.Length < n)
                {
                    info = info + " ";
                }
            }
            return info;
        }
        public Currency GetLowEstablishCurrency()
        {
            double purchasePrice= PurchasePrice;
            double sellingPrice= SellingPrice;
            double sizeOfStock= SizeOfStock;
            string name= Name;

            double price = PurchasePrice / SellingPrice -0.02;
            int n = 10;
            if (price < 2) n = 20;
            if (price < 0.2) n = 50;
            double BestClothest = 1000;
            if (IsGoodTrader)
            {
                return new Currency(purchasePrice, sellingPrice, sizeOfStock, name, IsGoodTrader);
            }
            for (int i = n; i > 0; --i)
            {
                double clothest = Math.Abs(price-Math.Truncate(price * i)/i);
                if (BestClothest >= clothest)
                {
                    BestClothest = clothest;
                    purchasePrice = Math.Truncate(price * i);
                    sellingPrice = i;
                }
            }
            return new Currency(purchasePrice,sellingPrice,sizeOfStock,name, IsGoodTrader);
        }
        public Currency GetUperEstablishCurrency()
        {
            double purchasePrice = PurchasePrice;
            double sellingPrice = SellingPrice;
            double sizeOfStock = SizeOfStock;
            string name = Name;

            double price = PurchasePrice / SellingPrice + 0.02;
            int n = 10;
            if (price < 2) n = 20;
            if (price < 0.2) n = 50;
            double BestClothest = 1000;
            if (IsGoodTrader)
            {
                return new Currency(purchasePrice, sellingPrice, sizeOfStock, name, IsGoodTrader);
            }
            for (int i = n; i > 0; --i)
            {
                double clothest = Math.Abs(Math.Ceiling(price * i) / i)-price;
                if (BestClothest >= clothest)
                {
                    BestClothest = clothest;
                    purchasePrice = Math.Ceiling(price * i);
                    sellingPrice = i;
                }
            }
            return new Currency(purchasePrice, sellingPrice, sizeOfStock, name, IsGoodTrader);
        }
    }
}
