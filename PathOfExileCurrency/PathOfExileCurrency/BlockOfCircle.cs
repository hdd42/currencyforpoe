﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PathOfExileCurrency
{
    class BlockOfCircle
    {
        public List<Circle> Block;
        private string FileForShortInfo;
        private string FileForShortSortedInfo;
        private string FileForEsteblishPrice;
        private string FileSorortedEstablishPrice;
        public BlockOfCircle()
        {
            Block=new List<Circle>();
            FileForShortInfo = @".\CurrencyInfo\ShortInfo.txt";
            FileForShortSortedInfo = @".\CurrencyInfo\ShortSortedInfo.txt";
            FileForEsteblishPrice = @".\CurrencyInfo\EsteblishPriceInfo.txt";
            FileSorortedEstablishPrice= @".\CurrencyInfo\SorortedEstablishPriceInfo.txt";
        }

        public void GetNormalInfo()
        {
            for (int i = 0; i < Block.Count; ++i)
            {
                Console.WriteLine(Block[i].GetInfo());
                Console.WriteLine();
            }
        }
        public void GetShortInfo()
        {
            for (int i = 0; i < Block.Count; ++i)
            {
                Console.WriteLine(Block[i].GetReverseShortInfo());
                Console.WriteLine();
            }
        }
        public void GetShortSortedInfo()
        {
            List<Circle> SortedBlock=new List<Circle>();
            for (int i = 0; i < Block.Count; ++i)
            {
                SortedBlock.Add(Block[i]);
            }

            SortedBlock=SortedBlock.OrderByDescending(i => i.Profit).ToList();
            for (int i = 0; i < SortedBlock.Count; ++i)
            {
                Console.WriteLine(SortedBlock[i].GetReverseShortInfo());
                Console.WriteLine();
            }
        }
        public void GetShortEstablishPrice()
        {
            List<Circle> EstablishBlock = new List<Circle>();
            for (int i = 0; i < Block.Count; ++i)
            {
                EstablishBlock.Add(Block[i].GetEsteblishCircle());
            }
            for (int i = 0; i < EstablishBlock.Count; ++i)
            {
                Console.WriteLine(EstablishBlock[i].GetShortInfo());
                Console.WriteLine();
            }
        }

        public void GetEstablishPriceInFile()
        {
            using (StreamWriter sw = new StreamWriter(FileForEsteblishPrice, false, System.Text.Encoding.Default))
            {
                List<Circle> EstablishBlock = new List<Circle>();
                for (int i = 0; i < Block.Count; ++i)
                {
                    EstablishBlock.Add(Block[i].GetEsteblishCircle());
                }
                for (int i = 0; i < EstablishBlock.Count; ++i)
                {
                    sw.WriteLine(EstablishBlock[i].GetReverseShortInfo());
                }
            }
        }
        public void GetSortedEstablishPriceInFile()
        {
            using (StreamWriter sw = new StreamWriter(FileSorortedEstablishPrice, false, System.Text.Encoding.Default))
            {
                List<Circle> EstablishBlock = new List<Circle>();
                for (int i = 0; i < Block.Count; ++i)
                {
                    EstablishBlock.Add(Block[i].GetEsteblishCircle());
                }
                EstablishBlock = EstablishBlock.OrderByDescending(i => i.Profit).ToList();
                for (int i = 0; i < EstablishBlock.Count; ++i)
                {
                    if (EstablishBlock[i].Profit>0) sw.WriteLine(EstablishBlock[i].GetReverseShortInfo());
                }
            }
        }
        public void GetShortInfoInFile()
        {
            using (StreamWriter sw = new StreamWriter(FileForShortInfo, false, System.Text.Encoding.Default))
            {
                for (int i = 0; i < Block.Count; ++i)
                {
                    sw.WriteLine(Block[i].GetReverseShortInfo());
                }
            }
        }
        public void GetShortSortedInfoInFile()
        {
            using (StreamWriter sw = new StreamWriter(FileForShortSortedInfo, false, System.Text.Encoding.Default))
            {
                List<Circle> SortedBlock = new List<Circle>();
                for (int i = 0; i < Block.Count; ++i)
                {
                    SortedBlock.Add(Block[i]);
                }

                SortedBlock = SortedBlock.OrderByDescending(i => i.Profit).ToList();
                for (int i = 0; i < SortedBlock.Count; ++i)
                {
                    sw.WriteLine(SortedBlock[i].GetReverseShortInfo());
                }
            }
        }
        public void RealodAllFileOfInfo()
        {
            GetShortSortedInfoInFile();
            GetShortInfoInFile();
            GetEstablishPriceInFile();
            GetSortedEstablishPriceInFile();
        }
    }
}
