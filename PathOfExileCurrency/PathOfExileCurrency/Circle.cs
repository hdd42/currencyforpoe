﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PathOfExileCurrency
{
    class Circle
    {
        public Currency Wise;
        public Currency CounterWise;
        public string Name;
        public double Profit;
        public Circle(Currency conterWice,Currency wice)
        {
            Wise = wice;
            CounterWise = conterWice;
            Name = conterWice.Name;

            double profit = wice.PurchasePrice / wice.SellingPrice
                - conterWice.PurchasePrice / conterWice.SellingPrice;
            Profit= profit * conterWice.SellingPrice / (conterWice.PurchasePrice / conterWice.SellingPrice);
        }

        public string GetInfo()
        {
            return (Name+" : "+ Wise.GetInfo() + " -  "
                            + CounterWise.GetInfo() + " Profit: "
                        + String.Format("{0:F3}",Profit));
        }
        public string GetShortInfo()
        {
            return (Name + " : " + Wise.GetShortInfo() + " -  "
                            + CounterWise.GetShortInfo() + " Profit: "
                        + String.Format("{0:F3}", Profit));
        }

        public string GetReverseShortInfo()
        {
            return (Name + " : " + Wise.GetShortInfo() + " -  "
                            + CounterWise.GetReverseShortInfo() + " Profit: "
                        + String.Format("{0:F3}", Profit));
        }
        public Circle GetEsteblishCircle()
        {
            return new Circle(CounterWise.GetUperEstablishCurrency(),Wise.GetLowEstablishCurrency());
        }
    }
}
