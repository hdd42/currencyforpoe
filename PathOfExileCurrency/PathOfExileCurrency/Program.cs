﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
namespace PathOfExileCurrency
{
    class Program
    {
        static void Main(string[] args)
        {
            BlockOfCircle AllCircles = GetAllCircle(16);
            AllCircles.RealodAllFileOfInfo();
        }

        private static BlockOfCircle GetAllCircle(int Count)
        {
            Console.Write("In proces:");
            BlockOfCircle circles=new BlockOfCircle();
            string endUrl = "&have=";
            string league = "Legacy";
            string beginUrl = "http://currency.poe.trade/search?league=Hardcore+Legacy&online=x&want=";
            List<int> AllCurrency=GetAllCurrency();
            foreach (var currency in AllCurrency)
            {
                circles.Block.Add(oneCircle(endUrl, beginUrl, currency));
            }
            Console.WriteLine();
            return circles;
        }

        private static List<int> GetAllCurrency()
        {
            List<int> allCurrency=new List<int>();
            StreamReader text = new StreamReader(@"Currency.txt");
            while (true)
            {
                var readLine = text.ReadLine();
                if (readLine != null) allCurrency.Add(int.Parse(readLine.Split(' ')[0]));
                else break;
            }
            return allCurrency;
        }

        private static Circle oneCircle(string endUrl, string beginUrl,int number)
        {
            Console.Write(".");
            var PriceListSeller = GetHtml(beginUrl + number + endUrl + 4);
            var PriceListByuer = GetHtml(beginUrl + 4 + endUrl + number);

            var bLockSeller = new BlockOfCurrency(PriceListSeller[0]);
            var bLockBuyer = new BlockOfCurrency(PriceListByuer[0]);

            for (int j = 0; j < Math.Min(PriceListSeller.Count, PriceListByuer.Count); ++j)
            {
                bLockSeller.Block.Add(PriceListSeller[j]);
                bLockBuyer.Block.Add(PriceListByuer[j]);
            }
            bLockBuyer.Reverse();
            bLockBuyer.ReloadBestOffer();
            bLockSeller.ReloadBestOffer();
            return new Circle(bLockSeller.BestOfer, bLockBuyer.BestOfer);

        }
        private static List<Currency> GetHtml(string sUrl)
        {
            var wrGeturl = WebRequest.Create(sUrl);

            wrGeturl.Proxy = WebProxy.GetDefaultProxy();

            var objStream = wrGeturl.GetResponse().GetResponseStream();

            StreamReader objReader = new StreamReader(objStream);

            var parser = new AngleSharp.Parser.Html.HtmlParser();

            return GetPrice(parser.Parse(objReader.ReadToEnd()));
        }
        private static List<Currency> GetPrice(AngleSharp.Dom.Html.IHtmlDocument document)
        {
            var ListOfPrice = document.All.Where(m => m.HasAttribute("data-sellvalue")).ToList();
            string dataIds="";
            string name = "empty";
            if (ListOfPrice.Count>0) {
                dataIds = document
                .QuerySelector(".displayoffer-left img")
                .GetAttribute("src")
                .Split('/')[3];
                name = dataIds.Substring(0, dataIds.Length - 4);
            }

            //KAtya Fitcha
            if (name.Length < 21)
            {
                while (name.Length < 21)
                {
                    name = name + " ";
                }
            }
            List<Currency> price = new List<Currency>();

            foreach (var item in ListOfPrice)
            {
                var output = item.Attributes;
                output.ToList();
                double sellCount = Convert.ToDouble(output[3].Value.Replace('.', ','));
                double buyCount = Convert.ToDouble(output[5].Value.Replace('.', ','));
                double stock = 0;
                int size = 0;
                if (name != "Chaos_Orb            ") size = 10;
                if (output.Length > 7) stock = Convert.ToDouble(output[7].Value.Replace('.', ','));
                if (price.Count < 7 && (stock>size))
                {
                    price.Add(new Currency(sellCount, buyCount, stock, name, GoodTrader(output[1].Value)));
                }
            }
            return price;
        }

        private static Boolean GoodTrader(string Nick)
        {
            Boolean check = false;
            StreamReader fs = new StreamReader(@"GoodTrader.txt");
            string nick = "";
            while (nick != null)
            {
                nick = fs.ReadLine();
                if (nick == Nick) check = true;
            }
            return check;
        }
    }
}
