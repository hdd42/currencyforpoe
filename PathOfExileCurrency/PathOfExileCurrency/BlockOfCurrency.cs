﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngleSharp.Network.Default;
using System.IO;
namespace PathOfExileCurrency
{
    class BlockOfCurrency
    {
        public List<Currency> Block;
        public string Name;
        public Currency BestOfer;

        public BlockOfCurrency(Currency currency)
        {
            Block=new List<Currency>();
            Name = currency.Name;
            BestOfer = currency;
        }

        public void Reverse()
        {
            for (int i=0;i<Block.Count;++i)
            {
                Currency a = new Currency(
                    Block[i].SellingPrice, Block[i].PurchasePrice, Block[i].SizeOfStock, Block[i].Name, Block[i].IsGoodTrader);
                Block[i] = a;
            }
        }
        public void ReloadBestOffer()
        {
            Boolean check = false;
            BestOfer = Block[0];
            for (int i = 1; i < Block.Count; ++i)
            {
                if (BestOfer.GetPrice() / Block[i].GetPrice() > 1.07)
                {
                    BestOfer = Block[i];
                }
                else break;
            }

        }
    }
}
